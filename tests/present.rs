macro_rules! duration_test {
    ($($name:ident: $duration:expr, $language:expr, $rough:expr, $precise:expr,)+) => {
        $(#[test]
        fn $name() {
            let ht = HumanTime::from($duration).language($language);
            let rough = ht.to_text(Accuracy::Rough, Tense::Present);
            let precise = ht.to_text(Accuracy::Precise, Tense::Present);
            assert_eq!($rough, rough);
            assert_eq!($precise, precise);
        })+
    }
}

#[cfg(test)]
mod duration {
    use chrono::Duration;
    use chrono_humanize::{Accuracy, HumanTime, Tense, Language};

    // test_name: Duration expression, Language, "Rough text", "Precise text"
    duration_test! {
        now_en: Duration::zero(), Language::English, "now", "0 seconds",
        plus_1s_en: Duration::seconds(1), Language::English, "now", "1 second",
        minus_1s_en: Duration::seconds(-1), Language::English, "now", "1 second",
        plus_5s_en: Duration::seconds(5), Language::English, "now", "5 seconds",
        minus_5s_en: Duration::seconds(-5), Language::English, "now", "5 seconds",
        plus_15s_en: Duration::seconds(15), Language::English, "15 seconds", "15 seconds",
        minus_15s_en: Duration::seconds(-15), Language::English, "15 seconds", "15 seconds",
        plus_95s_en: Duration::seconds(95), Language::English, "2 minutes", "1 minute and 35 seconds",
        minus_95s_en: Duration::seconds(-95), Language::English, "2 minutes", "1 minute and 35 seconds",
        plus_125s_en: Duration::seconds(125), Language::English, "2 minutes", "2 minutes and 5 seconds",
        minus_125s_en: Duration::seconds(-125), Language::English, "2 minutes", "2 minutes and 5 seconds",
        plus_31m_en: Duration::minutes(31), Language::English, "31 minutes", "31 minutes",
        minus_31m_en: Duration::minutes(-31), Language::English, "31 minutes", "31 minutes",
        plus_45m_en: Duration::minutes(45), Language::English, "45 minutes", "45 minutes",
        minus_45m_en: Duration::minutes(-45), Language::English, "45 minutes", "45 minutes",
        plus_46m_en: Duration::minutes(46), Language::English, "an hour", "46 minutes",
        minus_46m_en: Duration::minutes(-46), Language::English, "an hour", "46 minutes",
        plus_1h_en: Duration::hours(1), Language::English, "an hour", "1 hour",
        minus_1h_en: Duration::hours(-1), Language::English, "an hour", "1 hour",
        plus_12h_en: Duration::hours(12), Language::English, "12 hours", "12 hours",
        minus_12h_en: Duration::hours(-12), Language::English, "12 hours", "12 hours",
        plus_23h_en: Duration::hours(23), Language::English, "a day", "23 hours",
        minus_23h_en: Duration::hours(-23), Language::English, "a day", "23 hours",
        plus_26h_en: Duration::hours(26), Language::English, "a day", "1 day and 2 hours",
        minus_26h_en: Duration::hours(-26), Language::English, "a day", "1 day and 2 hours",
        plus_1d_en: Duration::days(1), Language::English, "a day", "1 day",
        minus_1d_en: Duration::days(-1), Language::English, "a day", "1 day",
        plus_2d_en: Duration::days(2), Language::English, "2 days", "2 days",
        minus_2d_en: Duration::days(-2), Language::English, "2 days", "2 days",
        plus_6d_13h_en: Duration::days(6) + Duration::hours(13), Language::English, "a week", "6 days and 13 hours",
        minus_6d_13h_en: Duration::days(-6) + Duration::hours(-13), Language::English, "a week", "6 days and 13 hours",
        plus_7d_en: Duration::days(7), Language::English, "a week", "1 week",
        minus_7d_en: Duration::days(-7), Language::English, "a week", "1 week",
        plus_10d_en: Duration::days(10), Language::English, "a week", "1 week and 3 days",
        minus_10d_en: Duration::days(-10), Language::English, "a week", "1 week and 3 days",
        plus_11d_en: Duration::days(11), Language::English, "2 weeks", "1 week and 4 days",
        minus_11d_en: Duration::days(-11), Language::English, "2 weeks", "1 week and 4 days",
        plus_4w_en: Duration::weeks(4), Language::English, "4 weeks", "4 weeks",
        minus_4w_en: Duration::weeks(-4), Language::English, "4 weeks", "4 weeks",
        plus_30d_en: Duration::days(30), Language::English, "a month", "1 month",
        minus_30d_en: Duration::days(-30), Language::English, "a month", "1 month",
        plus_45d_en: Duration::days(45), Language::English, "a month", "1 month, 2 weeks and 1 day",
        minus_45d_en: Duration::days(-45), Language::English, "a month", "1 month, 2 weeks and 1 day",
        plus_46d_en: Duration::days(46), Language::English, "2 months", "1 month, 2 weeks and 2 days",
        minus_46d_en: Duration::days(-46), Language::English, "2 months", "1 month, 2 weeks and 2 days",
        plus_24w_en: Duration::weeks(24), Language::English, "5 months", "5 months, 2 weeks and 4 days",
        minus_24w_en: Duration::weeks(-24), Language::English, "5 months", "5 months, 2 weeks and 4 days",
        plus_26w_en: Duration::weeks(26), Language::English, "6 months", "6 months and 2 days",
        minus_26w_en: Duration::weeks(-26), Language::English, "6 months", "6 months and 2 days",
        plus_50w_en: Duration::weeks(50), Language::English, "a year", "11 months, 2 weeks and 6 days",
        minus_50w_en: Duration::weeks(-50), Language::English, "a year", "11 months, 2 weeks and 6 days",
        plus_100w_en: Duration::weeks(100), Language::English, "2 years", "1 year, 11 months and 5 days",
        minus_100w_en: Duration::weeks(-100), Language::English, "2 years", "1 year, 11 months and 5 days",
        plus_101w_en: Duration::weeks(101), Language::English, "2 years", "1 year, 11 months, 1 week and 5 days",
        minus_101w_en: Duration::weeks(-101), Language::English, "2 years", "1 year, 11 months, 1 week and 5 days",
        plus_120w_en: Duration::weeks(120), Language::English, "2 years", "2 years, 3 months, 2 weeks and 6 days",
        minus_120w_en: Duration::weeks(-120), Language::English, "2 years", "2 years, 3 months, 2 weeks and 6 days",
        plus_200w_en: Duration::weeks(200), Language::English, "3 years", "3 years, 10 months and 5 days",
        minus_200w_en: Duration::weeks(-200), Language::English, "3 years", "3 years, 10 months and 5 days",

        now_pt: Duration::zero(), Language::Portuguese, "agora", "0 segundos",
        plus_1s_pt: Duration::seconds(1), Language::Portuguese, "agora", "1 segundo",
        minus_1s_pt: Duration::seconds(-1), Language::Portuguese, "agora", "1 segundo",
        plus_5s_pt: Duration::seconds(5), Language::Portuguese, "agora", "5 segundos",
        minus_5s_pt: Duration::seconds(-5), Language::Portuguese, "agora", "5 segundos",
        plus_15s_pt: Duration::seconds(15), Language::Portuguese, "15 segundos", "15 segundos",
        minus_15s_pt: Duration::seconds(-15), Language::Portuguese, "15 segundos", "15 segundos",
        plus_95s_pt: Duration::seconds(95), Language::Portuguese, "2 minutos", "1 minuto e 35 segundos",
        minus_95s_pt: Duration::seconds(-95), Language::Portuguese, "2 minutos", "1 minuto e 35 segundos",
        plus_125s_pt: Duration::seconds(125), Language::Portuguese, "2 minutos", "2 minutos e 5 segundos",
        minus_125s_pt: Duration::seconds(-125), Language::Portuguese, "2 minutos", "2 minutos e 5 segundos",
        plus_31m_pt: Duration::minutes(31), Language::Portuguese, "31 minutos", "31 minutos",
        minus_31m_pt: Duration::minutes(-31), Language::Portuguese, "31 minutos", "31 minutos",
        plus_45m_pt: Duration::minutes(45), Language::Portuguese, "45 minutos", "45 minutos",
        minus_45m_pt: Duration::minutes(-45), Language::Portuguese, "45 minutos", "45 minutos",
        plus_46m_pt: Duration::minutes(46), Language::Portuguese, "uma hora", "46 minutos",
        minus_46m_pt: Duration::minutes(-46), Language::Portuguese, "uma hora", "46 minutos",
        plus_1h_pt: Duration::hours(1), Language::Portuguese, "uma hora", "1 hora",
        minus_1h_pt: Duration::hours(-1), Language::Portuguese, "uma hora", "1 hora",
        plus_12h_pt: Duration::hours(12), Language::Portuguese, "12 horas", "12 horas",
        minus_12h_pt: Duration::hours(-12), Language::Portuguese, "12 horas", "12 horas",
        plus_23h_pt: Duration::hours(23), Language::Portuguese, "um dia", "23 horas",
        minus_23h_pt: Duration::hours(-23), Language::Portuguese, "um dia", "23 horas",
        plus_26h_pt: Duration::hours(26), Language::Portuguese, "um dia", "1 dia e 2 horas",
        minus_26h_pt: Duration::hours(-26), Language::Portuguese, "um dia", "1 dia e 2 horas",
        plus_1d_pt: Duration::days(1), Language::Portuguese, "um dia", "1 dia",
        minus_1d_pt: Duration::days(-1), Language::Portuguese, "um dia", "1 dia",
        plus_2d_pt: Duration::days(2), Language::Portuguese, "2 dias", "2 dias",
        minus_2d_pt: Duration::days(-2), Language::Portuguese, "2 dias", "2 dias",
        plus_6d_13h_pt: Duration::days(6) + Duration::hours(13), Language::Portuguese, "uma semana", "6 dias e 13 horas",
        minus_6d_13h_pt: Duration::days(-6) + Duration::hours(-13), Language::Portuguese, "uma semana", "6 dias e 13 horas",
        plus_7d_pt: Duration::days(7), Language::Portuguese, "uma semana", "1 semana",
        minus_7d_pt: Duration::days(-7), Language::Portuguese, "uma semana", "1 semana",
        plus_10d_pt: Duration::days(10), Language::Portuguese, "uma semana", "1 semana e 3 dias",
        minus_10d_pt: Duration::days(-10), Language::Portuguese, "uma semana", "1 semana e 3 dias",
        plus_11d_pt: Duration::days(11), Language::Portuguese, "2 semanas", "1 semana e 4 dias",
        minus_11d_pt: Duration::days(-11), Language::Portuguese, "2 semanas", "1 semana e 4 dias",
        plus_4w_pt: Duration::weeks(4), Language::Portuguese, "4 semanas", "4 semanas",
        minus_4w_pt: Duration::weeks(-4), Language::Portuguese, "4 semanas", "4 semanas",
        plus_30d_pt: Duration::days(30), Language::Portuguese, "um mês", "1 mês",
        minus_30d_pt: Duration::days(-30), Language::Portuguese, "um mês", "1 mês",
        plus_45d_pt: Duration::days(45), Language::Portuguese, "um mês", "1 mês, 2 semanas e 1 dia",
        minus_45d_pt: Duration::days(-45), Language::Portuguese, "um mês", "1 mês, 2 semanas e 1 dia",
        plus_46d_pt: Duration::days(46), Language::Portuguese, "2 meses", "1 mês, 2 semanas e 2 dias",
        minus_46d_pt: Duration::days(-46), Language::Portuguese, "2 meses", "1 mês, 2 semanas e 2 dias",
        plus_24w_pt: Duration::weeks(24), Language::Portuguese, "5 meses", "5 meses, 2 semanas e 4 dias",
        minus_24w_pt: Duration::weeks(-24), Language::Portuguese, "5 meses", "5 meses, 2 semanas e 4 dias",
        plus_26w_pt: Duration::weeks(26), Language::Portuguese, "6 meses", "6 meses e 2 dias",
        minus_26w_pt: Duration::weeks(-26), Language::Portuguese, "6 meses", "6 meses e 2 dias",
        plus_50w_pt: Duration::weeks(50), Language::Portuguese, "um ano", "11 meses, 2 semanas e 6 dias",
        minus_50w_pt: Duration::weeks(-50), Language::Portuguese, "um ano", "11 meses, 2 semanas e 6 dias",
        plus_100w_pt: Duration::weeks(100), Language::Portuguese, "2 anos", "1 ano, 11 meses e 5 dias",
        minus_100w_pt: Duration::weeks(-100), Language::Portuguese, "2 anos", "1 ano, 11 meses e 5 dias",
        plus_101w_pt: Duration::weeks(101), Language::Portuguese, "2 anos", "1 ano, 11 meses, 1 semana e 5 dias",
        minus_101w_pt: Duration::weeks(-101), Language::Portuguese, "2 anos", "1 ano, 11 meses, 1 semana e 5 dias",
        plus_120w_pt: Duration::weeks(120), Language::Portuguese, "2 anos", "2 anos, 3 meses, 2 semanas e 6 dias",
        minus_120w_pt: Duration::weeks(-120), Language::Portuguese, "2 anos", "2 anos, 3 meses, 2 semanas e 6 dias",
        plus_200w_pt: Duration::weeks(200), Language::Portuguese, "3 anos", "3 anos, 10 meses e 5 dias",
        minus_200w_pt: Duration::weeks(-200), Language::Portuguese, "3 anos", "3 anos, 10 meses e 5 dias",
    }
}

#[cfg(test)]
mod utc {
    use chrono::Utc;
    use chrono_humanize::{Accuracy, HumanTime, Tense, Language};

    #[test]
    fn now() {
        let ht = HumanTime::from(Utc::now());
        let rough_en = ht.language(Language::English).to_text(Accuracy::Rough, Tense::Present);
        let rough_pt = ht.language(Language::Portuguese).to_text(Accuracy::Rough, Tense::Present);
        assert_eq!("now", rough_en);
        assert_eq!("agora", rough_pt);
    }
}

#[cfg(test)]
mod local {
    use chrono::{Duration, Local};
    use chrono_humanize::{Accuracy, HumanTime, Tense, Language};

    #[test]
    fn now() {
        let ht = HumanTime::from(Local::now());
        let rough_en = ht.language(Language::English).to_text(Accuracy::Rough, Tense::Present);
        let rough_pt = ht.language(Language::Portuguese).to_text(Accuracy::Rough, Tense::Present);
        assert_eq!("now", rough_en);
        assert_eq!("agora", rough_pt);
    }

    #[test]
    fn minus_35d() {
        let past = Local::now() - Duration::days(35);
        let ht = HumanTime::from(past);
        let rough_en = ht.language(Language::English).to_text(Accuracy::Rough, Tense::Present);
        let rough_pt = ht.language(Language::Portuguese).to_text(Accuracy::Rough, Tense::Present);
        assert_eq!("a month", rough_en);
        assert_eq!("um mês", rough_pt);
    }

    #[test]
    fn plus_35d() {
        let future = Local::now() + Duration::days(35);
        let ht = HumanTime::from(future);
        let rough_en = ht.language(Language::English).to_text(Accuracy::Rough, Tense::Present);
        let rough_pt = ht.language(Language::Portuguese).to_text(Accuracy::Rough, Tense::Present);
        assert_eq!("a month", rough_en);
        assert_eq!("um mês", rough_pt);
    }
}
