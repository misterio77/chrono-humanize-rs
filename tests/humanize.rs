extern crate chrono;
extern crate chrono_humanize;

#[cfg(test)]
mod duration {
    use chrono::Duration;
    use chrono_humanize::{Humanize, Language};

    #[test]
    fn now() {
        let english = Duration::zero().humanize();
        let portuguese = Duration::zero().humanize_in(Language::Portuguese);
        assert_eq!("now", english);
        assert_eq!("agora", portuguese);
    }

    #[test]
    fn plus_5s() {
        let english = Duration::seconds(5).humanize();
        let portuguese = Duration::seconds(5).humanize_in(Language::Portuguese);
        assert_eq!("now", english);
        assert_eq!("agora", portuguese);
    }

    #[test]
    fn minus_5s() {
        let english = Duration::seconds(-5).humanize();
        let portuguese = Duration::seconds(-5).humanize_in(Language::Portuguese);
        assert_eq!("now", english);
        assert_eq!("agora", portuguese);
    }

    #[test]
    fn plus_15s() {
        let english = Duration::seconds(15).humanize();
        let portuguese = Duration::seconds(15).humanize_in(Language::Portuguese);
        assert_eq!("in 15 seconds", english);
        assert_eq!("daqui a 15 segundos", portuguese);
    }

    #[test]
    fn minus_15s() {
        let english = Duration::seconds(-15).humanize();
        let portuguese = Duration::seconds(-15).humanize_in(Language::Portuguese);
        assert_eq!("15 seconds ago", english);
        assert_eq!("15 segundos atrás", portuguese);
    }

    #[test]
    fn plus_95s() {
        let english = Duration::seconds(95).humanize();
        let portuguese = Duration::seconds(95).humanize_in(Language::Portuguese);
        assert_eq!("in 2 minutes", english);
        assert_eq!("daqui a 2 minutos", portuguese);
    }

    #[test]
    fn minus_95s() {
        let english = Duration::seconds(-95).humanize();
        let portuguese = Duration::seconds(-95).humanize_in(Language::Portuguese);
        assert_eq!("2 minutes ago", english);
        assert_eq!("2 minutos atrás", portuguese);
    }

    #[test]
    fn plus_125s() {
        let english = Duration::seconds(125).humanize();
        let portuguese = Duration::seconds(125).humanize_in(Language::Portuguese);
        assert_eq!("in 2 minutes", english);
        assert_eq!("daqui a 2 minutos", portuguese);
    }

    #[test]
    fn minus_125s() {
        let english = Duration::seconds(-125).humanize();
        let portuguese = Duration::seconds(-125).humanize_in(Language::Portuguese);
        assert_eq!("2 minutes ago", english);
        assert_eq!("2 minutos atrás", portuguese);
    }

    #[test]
    fn plus_31m() {
        let english = Duration::minutes(31).humanize();
        let portuguese = Duration::minutes(31).humanize_in(Language::Portuguese);
        assert_eq!("in 31 minutes", english);
        assert_eq!("daqui a 31 minutos", portuguese);
    }

    #[test]
    fn minus_31m() {
        let english = Duration::minutes(-31).humanize();
        let portuguese = Duration::minutes(-31).humanize_in(Language::Portuguese);
        assert_eq!("31 minutes ago", english);
        assert_eq!("31 minutos atrás", portuguese);
    }

    #[test]
    fn plus_45m() {
        let english = Duration::minutes(45).humanize();
        let portuguese = Duration::minutes(45).humanize_in(Language::Portuguese);
        assert_eq!("in 45 minutes", english);
        assert_eq!("daqui a 45 minutos", portuguese);
    }

    #[test]
    fn minus_45m() {
        let english = Duration::minutes(-45).humanize();
        let portuguese = Duration::minutes(-45).humanize_in(Language::Portuguese);
        assert_eq!("45 minutes ago", english);
        assert_eq!("45 minutos atrás", portuguese);
    }

    #[test]
    fn plus_46m() {
        let english = Duration::minutes(46).humanize();
        let portuguese = Duration::minutes(46).humanize_in(Language::Portuguese);
        assert_eq!("in an hour", english);
        assert_eq!("daqui a uma hora", portuguese);
    }

    #[test]
    fn minus_46m() {
        let english = Duration::minutes(-46).humanize();
        let portuguese = Duration::minutes(-46).humanize_in(Language::Portuguese);
        assert_eq!("an hour ago", english);
        assert_eq!("uma hora atrás", portuguese);
    }

    #[test]
    fn plus_1h() {
        let english = Duration::hours(1).humanize();
        let portuguese = Duration::hours(1).humanize_in(Language::Portuguese);
        assert_eq!("in an hour", english);
        assert_eq!("daqui a uma hora", portuguese);
    }

    #[test]
    fn minus_1h() {
        let english = Duration::hours(-1).humanize();
        let portuguese = Duration::hours(-1).humanize_in(Language::Portuguese);
        assert_eq!("an hour ago", english);
        assert_eq!("uma hora atrás", portuguese);
    }

    #[test]
    fn plus_12h() {
        let english = Duration::hours(12).humanize();
        let portuguese = Duration::hours(12).humanize_in(Language::Portuguese);
        assert_eq!("in 12 hours", english);
        assert_eq!("daqui a 12 horas", portuguese);
    }

    #[test]
    fn minus_12h() {
        let english = Duration::hours(-12).humanize();
        let portuguese = Duration::hours(-12).humanize_in(Language::Portuguese);
        assert_eq!("12 hours ago", english);
        assert_eq!("12 horas atrás", portuguese);
    }

    #[test]
    fn plus_23h() {
        let english = Duration::hours(23).humanize();
        let portuguese = Duration::hours(23).humanize_in(Language::Portuguese);
        assert_eq!("in a day", english);
        assert_eq!("daqui a um dia", portuguese);
    }

    #[test]
    fn minus_23h() {
        let english = Duration::hours(-23).humanize();
        let portuguese = Duration::hours(-23).humanize_in(Language::Portuguese);
        assert_eq!("a day ago", english);
        assert_eq!("um dia atrás", portuguese);
    }

    #[test]
    fn plus_26h() {
        let english = Duration::hours(26).humanize();
        let portuguese = Duration::hours(26).humanize_in(Language::Portuguese);
        assert_eq!("in a day", english);
        assert_eq!("daqui a um dia", portuguese);
    }

    #[test]
    fn minus_26h() {
        let english = Duration::hours(-26).humanize();
        let portuguese = Duration::hours(-26).humanize_in(Language::Portuguese);
        assert_eq!("a day ago", english);
        assert_eq!("um dia atrás", portuguese);
    }

    #[test]
    fn plus_1d() {
        let english = Duration::days(1).humanize();
        let portuguese = Duration::days(1).humanize_in(Language::Portuguese);
        assert_eq!("in a day", english);
        assert_eq!("daqui a um dia", portuguese);
    }

    #[test]
    fn minus_1d() {
        let english = Duration::days(-1).humanize();
        let portuguese = Duration::days(-1).humanize_in(Language::Portuguese);
        assert_eq!("a day ago", english);
        assert_eq!("um dia atrás", portuguese);
    }

    #[test]
    fn plus_2d() {
        let english = Duration::days(2).humanize();
        let portuguese = Duration::days(2).humanize_in(Language::Portuguese);
        assert_eq!("in 2 days", english);
        assert_eq!("daqui a 2 dias", portuguese);
    }

    #[test]
    fn minus_2d() {
        let english = Duration::days(-2).humanize();
        let portuguese = Duration::days(-2).humanize_in(Language::Portuguese);
        assert_eq!("2 days ago", english);
        assert_eq!("2 dias atrás", portuguese);
    }

    #[test]
    fn plus_6d_13h() {
        let english = (Duration::days(6) + Duration::hours(13)).humanize();
        let portuguese = (Duration::days(6) + Duration::hours(13)).humanize_in(Language::Portuguese);
        assert_eq!("in a week", english);
        assert_eq!("daqui a uma semana", portuguese);
    }

    #[test]
    fn minus_6d_13h() {
        let english = (Duration::days(-6) + Duration::hours(-13)).humanize();
        let portuguese = (Duration::days(-6) + Duration::hours(-13)).humanize_in(Language::Portuguese);
        assert_eq!("a week ago", english);
        assert_eq!("uma semana atrás", portuguese);
    }

    #[test]
    fn plus_7d() {
        let english = Duration::days(7).humanize();
        let portuguese = Duration::days(7).humanize_in(Language::Portuguese);
        assert_eq!("in a week", english);
        assert_eq!("daqui a uma semana", portuguese);
    }

    #[test]
    fn minus_7d() {
        let english = Duration::days(-7).humanize();
        let portuguese = Duration::days(-7).humanize_in(Language::Portuguese);
        assert_eq!("a week ago", english);
        assert_eq!("uma semana atrás", portuguese);
    }

    #[test]
    fn plus_10d() {
        let english = Duration::days(10).humanize();
        let portuguese = Duration::days(10).humanize_in(Language::Portuguese);
        assert_eq!("in a week", english);
        assert_eq!("daqui a uma semana", portuguese);
    }

    #[test]
    fn minus_10d() {
        let english = Duration::days(-10).humanize();
        let portuguese = Duration::days(-10).humanize_in(Language::Portuguese);
        assert_eq!("a week ago", english);
        assert_eq!("uma semana atrás", portuguese);
    }

    #[test]
    fn plus_11d() {
        let english = Duration::days(11).humanize();
        let portuguese = Duration::days(11).humanize_in(Language::Portuguese);
        assert_eq!("in 2 weeks", english);
        assert_eq!("daqui a 2 semanas", portuguese);
    }

    #[test]
    fn minus_11d() {
        let english = Duration::days(-11).humanize();
        let portuguese = Duration::days(-11).humanize_in(Language::Portuguese);
        assert_eq!("2 weeks ago", english);
        assert_eq!("2 semanas atrás", portuguese);
    }

    #[test]
    fn plus_4w() {
        let english = Duration::weeks(4).humanize();
        let portuguese = Duration::weeks(4).humanize_in(Language::Portuguese);
        assert_eq!("in 4 weeks", english);
        assert_eq!("daqui a 4 semanas", portuguese);
    }

    #[test]
    fn minus_4w() {
        let english = Duration::weeks(-4).humanize();
        let portuguese = Duration::weeks(-4).humanize_in(Language::Portuguese);
        assert_eq!("4 weeks ago", english);
        assert_eq!("4 semanas atrás", portuguese);
    }

    #[test]
    fn plus_30d() {
        let english = Duration::days(30).humanize();
        let portuguese = Duration::days(30).humanize_in(Language::Portuguese);
        assert_eq!("in a month", english);
        assert_eq!("daqui a um mês", portuguese);
    }

    #[test]
    fn minus_30d() {
        let english = Duration::days(-30).humanize();
        let portuguese = Duration::days(-30).humanize_in(Language::Portuguese);
        assert_eq!("a month ago", english);
        assert_eq!("um mês atrás", portuguese);
    }

    #[test]
    fn plus_45d() {
        let english = Duration::days(45).humanize();
        let portuguese = Duration::days(45).humanize_in(Language::Portuguese);
        assert_eq!("in a month", english);
        assert_eq!("daqui a um mês", portuguese);
    }

    #[test]
    fn minus_45d() {
        let english = Duration::days(-45).humanize();
        let portuguese = Duration::days(-45).humanize_in(Language::Portuguese);
        assert_eq!("a month ago", english);
        assert_eq!("um mês atrás", portuguese);
    }

    #[test]
    fn plus_46d() {
        let english = Duration::days(46).humanize();
        let portuguese = Duration::days(46).humanize_in(Language::Portuguese);
        assert_eq!("in 2 months", english);
        assert_eq!("daqui a 2 meses", portuguese);
    }

    #[test]
    fn minus_46d() {
        let english = Duration::days(-46).humanize();
        let portuguese = Duration::days(-46).humanize_in(Language::Portuguese);
        assert_eq!("2 months ago", english);
        assert_eq!("2 meses atrás", portuguese);
    }

    #[test]
    fn plus_24w() {
        let english = Duration::weeks(24).humanize();
        let portuguese = Duration::weeks(24).humanize_in(Language::Portuguese);
        assert_eq!("in 5 months", english);
        assert_eq!("daqui a 5 meses", portuguese);
    }

    #[test]
    fn minus_24w() {
        let english = Duration::weeks(-24).humanize();
        let portuguese = Duration::weeks(-24).humanize_in(Language::Portuguese);
        assert_eq!("5 months ago", english);
        assert_eq!("5 meses atrás", portuguese);
    }

    #[test]
    fn plus_26w() {
        let english = Duration::weeks(26).humanize();
        let portuguese = Duration::weeks(26).humanize_in(Language::Portuguese);
        assert_eq!("in 6 months", english);
        assert_eq!("daqui a 6 meses", portuguese);
    }

    #[test]
    fn minus_26w() {
        let english = Duration::weeks(-26).humanize();
        let portuguese = Duration::weeks(-26).humanize_in(Language::Portuguese);
        assert_eq!("6 months ago", english);
        assert_eq!("6 meses atrás", portuguese);
    }

    #[test]
    fn plus_50w() {
        let english = Duration::weeks(50).humanize();
        let portuguese = Duration::weeks(50).humanize_in(Language::Portuguese);
        assert_eq!("in a year", english);
        assert_eq!("daqui a um ano", portuguese);
    }

    #[test]
    fn minus_50w() {
        let english = Duration::weeks(-50).humanize();
        let portuguese = Duration::weeks(-50).humanize_in(Language::Portuguese);
        assert_eq!("a year ago", english);
        assert_eq!("um ano atrás", portuguese);
    }

    #[test]
    fn plus_100w() {
        let english = Duration::weeks(100).humanize();
        let portuguese = Duration::weeks(100).humanize_in(Language::Portuguese);
        assert_eq!("in 2 years", english);
        assert_eq!("daqui a 2 anos", portuguese);
    }

    #[test]
    fn minus_100w() {
        let english = Duration::weeks(-100).humanize();
        let portuguese = Duration::weeks(-100).humanize_in(Language::Portuguese);
        assert_eq!("2 years ago", english);
        assert_eq!("2 anos atrás", portuguese);
    }

    #[test]
    fn plus_120w() {
        let english = Duration::weeks(120).humanize();
        let portuguese = Duration::weeks(120).humanize_in(Language::Portuguese);
        assert_eq!("in 2 years", english);
        assert_eq!("daqui a 2 anos", portuguese);
    }

    #[test]
    fn minus_120w() {
        let english = Duration::weeks(-120).humanize();
        let portuguese = Duration::weeks(-120).humanize_in(Language::Portuguese);
        assert_eq!("2 years ago", english);
        assert_eq!("2 anos atrás", portuguese);
    }

    #[test]
    fn plus_200w() {
        let english = Duration::weeks(200).humanize();
        let portuguese = Duration::weeks(200).humanize_in(Language::Portuguese);
        assert_eq!("in 3 years", english);
        assert_eq!("daqui a 3 anos", portuguese);
    }

    #[test]
    fn minus_200w() {
        let english = Duration::weeks(-200).humanize();
        let portuguese = Duration::weeks(-200).humanize_in(Language::Portuguese);
        assert_eq!("3 years ago", english);
        assert_eq!("3 anos atrás", portuguese);
    }
}

#[cfg(test)]
mod utc {
    use chrono::Utc;
    use chrono_humanize::{Humanize, Language};

    #[test]
    fn now() {
        let english = Utc::now().humanize();
        let portuguese = Utc::now().humanize_in(Language::Portuguese);
        assert_eq!("now", english);
        assert_eq!("agora", portuguese);
    }
}

#[cfg(test)]
mod local {
    use chrono::{Duration, Local};
    use chrono_humanize::{Humanize, Language};

    #[test]
    fn now() {
        let english = Local::now().humanize();
        let portuguese = Local::now().humanize_in(Language::Portuguese);
        assert_eq!("now", english);
        assert_eq!("agora", portuguese);
    }

    #[test]
    fn minus_35d() {
        let past = Local::now() - Duration::days(35);
        let english = past.humanize();
        let portuguese = past.humanize_in(Language::Portuguese);
        assert_eq!("a month ago", english);
        assert_eq!("um mês atrás", portuguese);
    }

    #[test]
    fn plus_35d() {
        let future = Local::now() + Duration::days(35);
        let english = future.humanize();
        let portuguese = future.humanize_in(Language::Portuguese);
        assert_eq!("in a month", english);
        assert_eq!("daqui a um mês", portuguese);
    }
}
